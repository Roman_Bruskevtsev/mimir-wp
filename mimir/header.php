<?php
/**
 * @package WordPress
 * @subpackage Mimir
 * @since 1.0
 * @version 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header class="mmr-header">
        <div class="container">
            <div class="row">
                <div class="col-6 col-lg-2">
                    <?php if( get_field('logo', 'option') ) { ?>
                    <a class="mmr-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo get_field('logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <div class="col-6 col-lg-10">
                    <?php if( has_nav_menu('main') ) { ?>
                    <div class="mmr-mobile__btn float-end d-block d-lg-none">
                        <span></span>
                        <span></span>
                    </div>
                    <div class="mmr-main__menu float-end d-none d-lg-block">
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'mmr-main__nav float-end'
                        ) ); ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </header>
    <?php if( has_nav_menu('main') ) { ?>
    <div class="mmr-mobile__menu d-block d-lg-none">
        <?php wp_nav_menu( array(
            'theme_location'        => 'main',
            'container'             => 'nav',
            'container_class'       => 'mmr-mobile__nav'
        ) ); ?>
    </div>
    <?php } ?>
    <main>