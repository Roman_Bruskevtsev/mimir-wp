<?php
/**
 * @package WordPress
 * @subpackage Mimir
 * @since 1.0
 * @version 1.0
 */
$footer = get_field('footer', 'option'); ?>  
    </main>
    <footer class="mmr-footer scroll__section" id="contact">
        <div class="mmr-footer__icon"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 order-lg-3">
                    <?php if( $footer['form_shortcode'] ) { ?>
                        <div class="mmr-footer__form" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500"><?php echo do_shortcode( $footer['form_shortcode'] ); ?></div>
                    <?php } ?>
                </div>
                <div class="col-lg-1 order-lg-2"></div>
                <div class="col-lg-4 order-lg-1">
                    <div class="mmr-footer__contacts" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
                        <?php if( $footer['title'] ) { ?>
                            <h6 class="title frostee"><?php echo $footer['title']; ?></h6>
                        <?php } 
                        if( $footer['address'] ) { ?>
                        <div class="mmr-footer__contacts__block">
                            <h6><?php _e('Location', 'mimir'); ?></h6>
                            <?php echo $footer['address']; ?>
                        </div>
                        <?php }
                        if( $footer['email'] ) { ?>
                        <div class="mmr-footer__contacts__block">
                            <h6><?php _e('Email', 'mimir'); ?></h6>
                            <a href="mailto:<?php echo $footer['email']; ?>"><?php echo $footer['email']; ?></a>
                        </div>
                        <?php } 
                        if( $footer['phone'] ) { ?>
                        <div class="mmr-footer__contacts__block">
                            <h6><?php _e('Contact number', 'mimir'); ?></h6>
                            <a href="tel:<?php echo $footer['phone']; ?>"><?php echo $footer['phone']; ?></a>
                        </div>
                        <?php } 
                        if( $footer['logo'] ) { ?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="mmr-footer__logo">
                            <img src="<?php echo $footer['logo']['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        </a>
                        <?php } 
                        if( $footer['copyrights'] ) { ?>
                        <div class="mmr-footer__copyrights"><p><?php echo $footer['copyrights']; ?></p></div>
                        <?php } ?>
                    </div>
                </div>
                
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>