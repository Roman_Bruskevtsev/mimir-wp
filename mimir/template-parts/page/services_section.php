<?php
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="mmr-services__section scroll__section"<?php echo $anchor; ?>>
	<div class="circles"></div>
	<div class="container lg-small-padding">
		<?php if( get_sub_field('small_title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="mmr-section__title text-center" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<h5 class="green"><?php the_sub_field('small_title'); ?></h5>
				</div>
			</div>	
		</div>
		<?php } ?>
		<?php if( get_sub_field('image') || get_sub_field('title') || get_sub_field('text') ) { ?>
		<div class="row">
			<?php if( get_sub_field('image') ) { ?>
			<div class="col-lg-6">
				<div class="image" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
			</div>
			<?php } 
			if( get_sub_field('title') || get_sub_field('text') ) { ?>
			<div class="col-lg-6">
				<div class="text" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<div class="mmr-section__title white">
						<?php if( get_sub_field('title') ) { ?>
							<h2><?php the_sub_field('title'); ?></h2>
						<?php } 
						the_sub_field('text'); ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } 
		$services = get_sub_field('services'); 
		if( $services ) { 
			$i = 1;	?>
		<div class="row">
			<div class="col">
				<div class="mmr-services__slider__wrapper" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<div class="reactangle top float-end"></div>
					<div class="mmr-services__slider__block">
						<div class="mmr-services__navigation">
							<?php foreach ( $services as $service ) { ?>
							<div class="mmr-services__item<?php echo $i == 1 ? ' active' : ''; ?>">
								<h6><?php echo $service['title']; ?></h6>
							</div>
							<?php $i++; } ?>
						</div>
						<div class="mmr-services__slider swiper">
							<div class="swiper-wrapper">
								<?php foreach ( $services as $service ) { ?>
								<div class="swiper-slide">
									<?php if( $service['title'] || $service['text'] ) { ?>
									<div class="text">
										<?php if( $service['title'] ) { ?>
											<h3><?php echo $service['title']; ?></h3>
										<?php } 
										echo $service['text']; ?>
									</div>
									<?php } 
									if( $service['icon'] ) { ?>
									<div class="icon text-center">
										<img src="<?php echo $service['icon']['url']; ?>" alt="<?php echo $service['icon']['title']; ?>">
									</div>
									<?php } ?>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="reactangle bottom float-start"></div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>