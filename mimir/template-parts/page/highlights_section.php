<?php
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="mmr-highlights__section scroll__section"<?php echo $anchor; ?>>
	<div class="container">
		<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="mmr-section__title text-center" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<?php if( get_sub_field('small_title') ) { ?>
						<h5 class="green mb-16"><?php the_sub_field('small_title'); ?></h5>
					<?php }
					if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
			</div>	
		</div>
		<?php }
		$blocks =  get_sub_field('blocks');
		if( $blocks ) { ?>
		<div class="row">
			<div class="col">
				<div class="mmr-highlights__slider swiper" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<div class="swiper-wrapper">
					<?php foreach ( $blocks as $block ) { ?>
						<div class="swiper-slide">
							<div class="mmr-highlights__reactangle top"></div>
							<div class="mmr-highlights__block text-center">
								<?php if( $block['icon'] ) { ?>
								<div class="icon">
									<img src="<?php echo $block['icon']['url']; ?>" alt="<?php echo $block['icon']['title']; ?>">
								</div>
								<?php } 
								if( $block['title'] ) { ?>
									<p><?php echo $block['title']; ?></p>
								<?php } ?>
							</div>
							<div class="mmr-highlights__reactangle bottom"></div>
						</div>
					<?php } ?>
					</div>
					<div class="swiper-pagination d-block d-xl-none"></div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>