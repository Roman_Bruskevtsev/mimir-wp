<?php 
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background').')"' : '';
?>
<section class="mmr-hero__section"<?php echo $background; ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="content__wrapper">
					<div class="content">
					<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
						<div class="mmr-section__title" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
							<?php if( get_sub_field('small_title') ) { ?>
								<h5 class="green mb-30"><?php the_sub_field('small_title'); ?></h5>
							<?php }
							if( get_sub_field('title') ) { ?>
								<h1><?php the_sub_field('title'); ?></h1>
							<?php } ?>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>