<?php
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background').')"' : '';
$anchor = get_sub_field('anchor') ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="mmr-about__us__section scroll__section"<?php echo $anchor.$background; ?>>
	<div class="circles"></div>
	<div class="container lg-nopadding">
		<div class="row justify-content-end">
			<?php if( get_sub_field('title') || get_sub_field('text') ) { ?>
			<div class="col-lg-6">
				<div class="mmr-about__us__wrapper" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<div class="reactangle top float-end"></div>
					<div class="mmr-about__us__block">
						<div class="mmr-section__title white">
							<?php if( get_sub_field('title') ) { ?>
								<h5 class="green"><?php the_sub_field('title'); ?></h5>
							<?php } 
							the_sub_field('text'); ?>
						</div>
					</div>
					<div class="reactangle bottom float-start"></div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>