'use strict';

class GeneralClass{
    constructor(){
        this.init();
    }

    init(){
        this.documentReady();
        this.windowLoad();
    }

    documentReady(){
        document.addEventListener('DOMContentLoaded', function(){
            AOS.init();
            general.headerAnimationInit();
            general.servicesSliderInit();
            general.highlightsSlider();
            general.mobileNavigation();
            
        });
    }
    windowLoad(){
        window.onload = function() {
            general.sectionScrollInit();
        }
    }

    headerAnimationInit(){
        let header = document.querySelector('.mmr-header'),
            lastScrollTop = 0,
            smoothMenuPoints = document.querySelectorAll('.mmr-main__nav li a[href^="#"]'),
            smoothMobileMenuPoints = document.querySelectorAll('.mmr-mobile__nav li a[href^="#"]'),
            st;

        window.addEventListener('scroll', () => {
            st = window.pageYOffset;
            
            if( st <= 0 ){
                header.classList.remove('scroll__down');
            } else if ( st > lastScrollTop && st > 0 ){
                header.classList.add('scroll__down');
            } else {
                header.classList.remove('scroll__down');
            }
            if( st > 100 ){
                header.classList.add('dark');
            } else {
                header.classList.remove('dark');
            }

            lastScrollTop = st;
        });

        if( smoothMenuPoints ){
            let headerHeight, pointHref, pointTopHeight, siteUrl, newUrl;
            smoothMenuPoints.forEach( (point) => {
                point.addEventListener('click', (e) => {
                    e.preventDefault();
                    headerHeight = header.offsetHeight;
                    pointHref = point.getAttribute('href');
                    pointTopHeight = document.querySelector(pointHref).getBoundingClientRect().top + window.scrollY;
                    siteUrl = document.URL.split('#')[0];
                    newUrl = siteUrl + pointHref;

                    window.history.pushState({}, '', newUrl);

                    window.scrollTo({
                        top: pointTopHeight,
                        behavior: "smooth"
                    });
                });
            });

            smoothMobileMenuPoints.forEach( (point) => {
                point.addEventListener('click', (e) => {
                    e.preventDefault();
                    headerHeight = header.offsetHeight;
                    pointHref = point.getAttribute('href');
                    pointTopHeight = document.querySelector(pointHref).getBoundingClientRect().top + window.scrollY;
                    siteUrl = document.URL.split('#')[0];
                    newUrl = siteUrl + pointHref;

                    window.history.pushState({}, '', newUrl);
                    
                    window.scrollTo({
                        top: pointTopHeight,
                        behavior: "smooth"
                    });
                });
            });
        }
    }

    mobileNavigation(){
        let btn = document.querySelector('.mmr-mobile__btn'),
            header = document.querySelector('.mmr-header'),
            menuPoints = document.querySelectorAll('.mmr-mobile__nav a'),
            menu = document.querySelector('.mmr-mobile__menu');
        if( !btn ) return false;
        btn.addEventListener('click', () => {
            btn.classList.toggle('show');
            header.classList.toggle('show');
            menu.classList.toggle('show');
        });

        menuPoints.forEach( (item) => {
            item.addEventListener('click', () => {
                menuPoints.forEach( (point) => {
                    point.classList.remove('active');
                });
                item.classList.add('active');
                header.classList.remove('show');
                btn.classList.remove('show');
                menu.classList.remove('show');
            });
        });
    }

    servicesSliderInit(){
        let slider = document.querySelector('.mmr-services__slider');
        if( !slider ) return false;
        let servicesSlider = new Swiper(slider, {
                slidesPerView: 1.25,
                spaceBetween: 27.5,
                speed: 1000,
                navigation: false,
                breakpoints: {
                    540: {
                        slidesPerView: 2,
                    },
                    992: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    },
                },
                // effect: 'fade',
                // autoplay: {
                //     delay: 5000,
                //     disableOnInteraction: false
                // },
                // navigation: {
                //     nextEl: '.swiper-button-next',
                //     prevEl: '.swiper-button-prev',
                // },
            }),
            navigationItems = slider.closest('.mmr-services__slider__block').querySelectorAll('.mmr-services__item'),
            itemIndex;

        navigationItems = Array.prototype.slice.call(navigationItems);

        navigationItems.forEach( (item) => {
            item.addEventListener('click', () => {
                navigationItems.forEach( (nav) => {
                    nav.classList.remove('active');
                });
                item.classList.add('active');

                itemIndex = navigationItems.indexOf(item);
                servicesSlider.slideTo(itemIndex, 1000, false);
                
            }); 
        });

        servicesSlider.on('slideChange', function () {
            navigationItems.forEach( (nav) => {
                nav.classList.remove('active');
            });
            navigationItems[this.activeIndex].classList.add('active');
            
            console.log(servicesSlider.activeIndex);
        });
    }

    highlightsSlider(){
        let slider = document.querySelector('.mmr-highlights__slider');
        if( !slider ) return false;
        let testimonialsSlider = new Swiper(slider, {
            slidesPerView: 1,
            speed: 1000,
            navigation: false,
            spaceBetween: 50,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false
            },
            pagination: {
                el: '.swiper-pagination',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2
                },
                991: {
                    slidesPerView: 3
                },
                1199: {
                    slidesPerView: 4
                }
            }
        });
    }

    sectionScrollInit(){
        let scrollSections = document.querySelectorAll('.scroll__section'),
            menuItems = document.querySelectorAll('.mmr-main__menu li'),
            header = document.querySelector('.mmr-header'),
            mobileMenuItems = document.querySelectorAll('.mmr-mobile__menu li'),
            topScroll, sectionScroll, sectionHeight, sectionActiveHeight, activeSection, linkHref, headerHeight;
        if(!scrollSections) return false;

        window.addEventListener('scroll', function() {
            topScroll = window.scrollY;
            headerHeight = header.offsetHeight;
            scrollSections.forEach( (section) => {
                sectionScroll = section.getBoundingClientRect().top;

                if( sectionScroll - headerHeight <= 0 ) {
                    activeSection = '#' + section.getAttribute('id');

                    menuItems.forEach( (item) => {
                        linkHref = item.querySelector('a').getAttribute('href');
                        linkHref = '#' + linkHref.substring(linkHref.indexOf('#') + 1);
                        console.log(linkHref, activeSection);
                        if( linkHref == activeSection ){
                            item.querySelector('a').classList.add('active');
                        } else {
                            item.querySelector('a').classList.remove('active');
                        }
                    });

                    mobileMenuItems.forEach( (item) => {
                        linkHref = item.querySelector('a').getAttribute('href');
                        linkHref = '#' + linkHref.substring(linkHref.indexOf('#') + 1);

                        if( linkHref == activeSection ){
                            item.querySelector('a').classList.add('active');
                        } else {
                            item.querySelector('a').classList.remove('active');
                        }
                    });
                }
            });
        });
    }
}

let general = new GeneralClass();