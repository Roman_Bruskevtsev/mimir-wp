-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Бер 01 2023 р., 15:17
-- Версія сервера: 10.3.13-MariaDB-log
-- Версія PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `mimir`
--

-- --------------------------------------------------------

--
-- Структура таблиці `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-10-27 15:13:20', '2021-10-27 15:13:20', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://mimir-wp/build', 'yes'),
(2, 'home', 'http://mimir-wp/build', 'yes'),
(3, 'blogname', 'Mimir', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'bruskevtsevr@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:95:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=44&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'mimir', 'yes'),
(41, 'stylesheet', 'mimir', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '49752', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '0', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(78, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '44', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '71', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1650899600', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'wp_force_deactivated_plugins', 'a:0:{}', 'yes'),
(99, 'initial_db_version', '49752', 'yes'),
(100, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(101, 'fresh_site', '0', 'yes'),
(102, 'widget_block', 'a:6:{i:2;a:1:{s:7:\"content\";s:19:\"<!-- wp:search /-->\";}i:3;a:1:{s:7:\"content\";s:154:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Recent Posts</h2><!-- /wp:heading --><!-- wp:latest-posts /--></div><!-- /wp:group -->\";}i:4;a:1:{s:7:\"content\";s:227:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Recent Comments</h2><!-- /wp:heading --><!-- wp:latest-comments {\"displayAvatar\":false,\"displayDate\":false,\"displayExcerpt\":false} /--></div><!-- /wp:group -->\";}i:5;a:1:{s:7:\"content\";s:146:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Archives</h2><!-- /wp:heading --><!-- wp:archives /--></div><!-- /wp:group -->\";}i:6;a:1:{s:7:\"content\";s:150:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Categories</h2><!-- /wp:heading --><!-- wp:categories /--></div><!-- /wp:group -->\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:3:{i:0;s:7:\"block-2\";i:1;s:7:\"block-3\";i:2;s:7:\"block-4\";}s:8:\"footer-1\";a:2:{i:0;s:7:\"block-5\";i:1;s:7:\"block-6\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:6:{i:1676891600;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1676906000;a:5:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1676906012;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1676906014;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1677165200;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(118, 'recovery_keys', 'a:0:{}', 'yes'),
(119, 'theme_mods_twentytwentyone', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1635347789;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:7:\"block-2\";i:1;s:7:\"block-3\";i:2;s:7:\"block-4\";}s:9:\"sidebar-2\";a:2:{i:0;s:7:\"block-5\";i:1;s:7:\"block-6\";}}}}', 'yes'),
(120, 'https_detection_errors', 'a:1:{s:23:\"ssl_verification_failed\";a:1:{i:0;s:24:\"SSL verification failed.\";}}', 'yes'),
(121, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:6:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.1.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.1.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.1.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.1.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.1.1\";s:7:\"version\";s:5:\"6.1.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.1\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.1.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.1.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.1.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.1.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.1.1\";s:7:\"version\";s:5:\"6.1.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.1\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-6.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-6.1.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-6.1-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-6.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:3:\"6.1\";s:7:\"version\";s:3:\"6.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.1\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.0.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.0.3-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.0.3\";s:7:\"version\";s:5:\"6.0.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.1\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.9.5.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.9.5.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.9.5-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.9.5-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.9.5\";s:7:\"version\";s:5:\"5.9.5\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.1\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:5;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.6.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.6.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.8.6-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.8.6-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.8.6-partial-1.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.8.6-rollback-1.zip\";}s:7:\"current\";s:5:\"5.8.6\";s:7:\"version\";s:5:\"5.8.6\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.1\";s:15:\"partial_version\";s:5:\"5.8.1\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1676891575;s:15:\"version_checked\";s:5:\"5.8.1\";s:12:\"translations\";a:0:{}}', 'no'),
(123, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1676891583;s:8:\"response\";a:7:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"5.0.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.5.0.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:60:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=2818463\";s:2:\"1x\";s:60:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=2818463\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.0\";s:6:\"tested\";s:5:\"6.1.1\";s:12:\"requires_php\";s:3:\"5.2\";}s:27:\"autoptimize/autoptimize.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/autoptimize\";s:4:\"slug\";s:11:\"autoptimize\";s:6:\"plugin\";s:27:\"autoptimize/autoptimize.php\";s:11:\"new_version\";s:5:\"3.1.5\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/autoptimize/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/autoptimize.3.1.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/autoptimize/assets/icon-256X256.png?rev=2211608\";s:2:\"1x\";s:64:\"https://ps.w.org/autoptimize/assets/icon-128x128.png?rev=1864142\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/autoptimize/assets/banner-772x250.jpg?rev=1315920\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";s:6:\"tested\";s:5:\"6.1.1\";s:12:\"requires_php\";s:3:\"5.6\";}s:22:\"cyr3lat/cyr-to-lat.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/cyr3lat\";s:4:\"slug\";s:7:\"cyr3lat\";s:6:\"plugin\";s:22:\"cyr3lat/cyr-to-lat.php\";s:11:\"new_version\";s:3:\"3.7\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/cyr3lat/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/cyr3lat.3.7.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:51:\"https://s.w.org/plugins/geopattern-icon/cyr3lat.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"2.3\";s:6:\"tested\";s:5:\"6.1.1\";s:12:\"requires_php\";b:0;}s:23:\"loco-translate/loco.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:28:\"w.org/plugins/loco-translate\";s:4:\"slug\";s:14:\"loco-translate\";s:6:\"plugin\";s:23:\"loco-translate/loco.php\";s:11:\"new_version\";s:5:\"2.6.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/loco-translate/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/loco-translate.2.6.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-256x256.png?rev=1000676\";s:2:\"1x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-128x128.png?rev=1000676\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/loco-translate/assets/banner-772x250.jpg?rev=745046\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.2\";s:6:\"tested\";s:5:\"6.0.3\";s:12:\"requires_php\";s:6:\"5.6.20\";s:14:\"upgrade_notice\";s:54:\"<ul>\n<li>Various improvements and bug fixes</li>\n</ul>\";}s:31:\"query-monitor/query-monitor.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/query-monitor\";s:4:\"slug\";s:13:\"query-monitor\";s:6:\"plugin\";s:31:\"query-monitor/query-monitor.php\";s:11:\"new_version\";s:6:\"3.11.1\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/query-monitor/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/query-monitor.3.11.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/query-monitor/assets/icon-256x256.png?rev=2301273\";s:2:\"1x\";s:58:\"https://ps.w.org/query-monitor/assets/icon.svg?rev=2056073\";s:3:\"svg\";s:58:\"https://ps.w.org/query-monitor/assets/icon.svg?rev=2056073\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/query-monitor/assets/banner-1544x500.png?rev=2457098\";s:2:\"1x\";s:68:\"https://ps.w.org/query-monitor/assets/banner-772x250.png?rev=2457098\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";s:6:\"tested\";s:5:\"6.1.1\";s:12:\"requires_php\";s:3:\"7.2\";}s:33:\"w3-total-cache/w3-total-cache.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/w3-total-cache\";s:4:\"slug\";s:14:\"w3-total-cache\";s:6:\"plugin\";s:33:\"w3-total-cache/w3-total-cache.php\";s:11:\"new_version\";s:5:\"2.3.0\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/w3-total-cache/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/w3-total-cache.2.3.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/w3-total-cache/assets/icon-256x256.png?rev=1041806\";s:2:\"1x\";s:67:\"https://ps.w.org/w3-total-cache/assets/icon-128x128.png?rev=1041806\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/w3-total-cache/assets/banner-772x250.jpg?rev=1041806\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.3\";s:6:\"tested\";s:5:\"6.1.1\";s:12:\"requires_php\";s:3:\"5.6\";}s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"6.0.7\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"6.1.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.7.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.7.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"6.0\";s:6:\"tested\";s:5:\"6.1.1\";s:12:\"requires_php\";b:0;}s:9:\"hello.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/hello-dolly/assets/banner-1544x500.jpg?rev=2645582\";s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.6\";}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:3:\"4.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/duplicate-post.4.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=2336666\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=2336666\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/duplicate-post/assets/banner-1544x500.png?rev=2336666\";s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=2336666\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.9\";s:6:\"tested\";s:5:\"6.1.1\";s:12:\"requires_php\";s:6:\"5.6.20\";}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"20.1\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.20.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2643727\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=2643727\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=2643727\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=2643727\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=2643727\";}s:8:\"requires\";s:3:\"6.0\";s:6:\"tested\";s:5:\"6.1.1\";s:12:\"requires_php\";s:6:\"5.6.20\";}}}', 'no'),
(126, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1676891585;s:7:\"checked\";a:4:{s:5:\"mimir\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"2.1\";s:12:\"twentytwenty\";s:3:\"1.8\";s:15:\"twentytwentyone\";s:3:\"1.4\";}s:8:\"response\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"2.4\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.2.4.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"2.1\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.2.1.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.7.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(134, 'can_compress_scripts', '1', 'no'),
(139, 'recently_activated', 'a:0:{}', 'yes'),
(146, 'acf_version', '5.10.2', 'yes'),
(147, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.5.2\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1635347630;s:7:\"version\";s:5:\"5.5.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(150, 'finished_updating_comment_type', '1', 'yes'),
(151, 'current_theme', 'Mimir', 'yes'),
(152, 'theme_mods_mimir', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:4:\"main\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(153, 'theme_switched', '', 'yes'),
(160, 'options_logo', '42', 'no'),
(161, '_options_logo', 'field_617993011e1bd', 'no'),
(162, 'options_footer_title', 'Contact', 'no'),
(163, '_options_footer_title', 'field_617993451e1c0', 'no'),
(164, 'options_footer_address', 'Unit #101, IFZA Dubai - Building A2,\r\nDubai Silicon Oasis, Dubai, UAE', 'no'),
(165, '_options_footer_address', 'field_6179935d1e1c1', 'no'),
(166, 'options_footer_email', 'info@mimir.com', 'no'),
(167, '_options_footer_email', 'field_6179936f1e1c2', 'no'),
(168, 'options_footer_phone', '+971561488717', 'no'),
(169, '_options_footer_phone', 'field_6179937e1e1c3', 'no'),
(170, 'options_footer_logo', '43', 'no'),
(171, '_options_footer_logo', 'field_617993921e1c4', 'no'),
(172, 'options_footer_copyrights', '© 2021 Mimir Technologies. All rights reserved.', 'no'),
(173, '_options_footer_copyrights', 'field_6179939d1e1c5', 'no'),
(174, 'options_footer_form_shortcode', '[contact-form-7 id=\"5\" title=\"Contact form\"]', 'no'),
(175, '_options_footer_form_shortcode', 'field_617993b21e1c6', 'no'),
(176, 'options_footer', '', 'no'),
(177, '_options_footer', 'field_617993371e1bf', 'no'),
(187, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(197, '_transient_health-check-site-status-result', '{\"good\":12,\"recommended\":4,\"critical\":3}', 'yes'),
(219, 'secret_key', 'k^gkGfOwXU%Ed$Q_VrC6c#&n}uEjZko*ybPtEM<YvWA6fK=L|(T*883OhQ*x4Q(]', 'no'),
(261, 'widget_recent-comments', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(262, 'widget_recent-posts', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(280, '_transient_timeout_acf_plugin_updates', '1677064384', 'no'),
(281, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"6.0.7\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"6.1.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.10.2\";}}', 'no'),
(282, '_site_transient_timeout_theme_roots', '1676893384', 'no'),
(283, '_site_transient_theme_roots', 'a:4:{s:5:\"mimir\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";}', 'no'),
(284, '_site_transient_timeout_php_check_a5b4d2808570efd012607394df5c6fa9', '1677496385', 'no'),
(285, '_site_transient_php_check_a5b4d2808570efd012607394df5c6fa9', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:0;}', 'no');

-- --------------------------------------------------------

--
-- Структура таблиці `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_form', '<div class=\"field__group\">[text* text-23 class:form__field placeholder \"Your Name\"]</div>\n<div class=\"field__group\">[email* email-89 class:form__field placeholder \"Your Email\"]</div>\n<div class=\"field__group\">[textarea* textarea-835 class:form__field placeholder \"Your Message\"]</div>\n<div class=\"submit__group\">[submit class:submit__field \"Send\"]</div>'),
(4, 5, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:34:\"[_site_title] <wordpress@mimir-wp>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:163:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(5, 5, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:34:\"[_site_title] <wordpress@mimir-wp>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:105:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(6, 5, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(7, 5, '_additional_settings', ''),
(8, 5, '_locale', 'en_US'),
(9, 6, '_edit_last', '1'),
(10, 6, '_edit_lock', '1635607251:1'),
(11, 8, '_edit_last', '1'),
(12, 8, '_edit_lock', '1635612881:1'),
(13, 42, '_wp_attached_file', '2021/10/logo.svg'),
(14, 43, '_wp_attached_file', '2021/10/footer-logo.svg'),
(15, 44, '_edit_lock', '1635712111:1'),
(16, 44, '_edit_last', '1'),
(17, 44, 'content', 'a:4:{i:0;s:12:\"hero_section\";i:1;s:16:\"about_us_section\";i:2;s:16:\"services_section\";i:3;s:18:\"highlights_section\";}'),
(18, 44, '_content', 'field_61796d5e7d4ce'),
(19, 45, 'content', ''),
(20, 45, '_content', 'field_61796d5e7d4ce'),
(21, 44, 'content_0_anchor', 'hero'),
(22, 44, '_content_0_anchor', 'field_61799135bfc81'),
(23, 44, 'content_0_small_title', 'Leading technologies for goal-oriented companies'),
(24, 44, '_content_0_small_title', 'field_61799125bfc7f'),
(25, 44, 'content_0_title', 'Software development partner to accelerate your business'),
(26, 44, '_content_0_title', 'field_6179912bbfc80'),
(27, 46, 'content', 'a:1:{i:0;s:12:\"hero_section\";}'),
(28, 46, '_content', 'field_61796d5e7d4ce'),
(29, 46, 'content_0_anchor', 'hero'),
(30, 46, '_content_0_anchor', 'field_61799135bfc81'),
(31, 46, 'content_0_small_title', 'Leading technologies for goal-oriented companies'),
(32, 46, '_content_0_small_title', 'field_61799125bfc7f'),
(33, 46, 'content_0_title', 'Software development partner to accelerate your business'),
(34, 46, '_content_0_title', 'field_6179912bbfc80'),
(35, 48, '_wp_attached_file', '2021/10/background-1-scaled.jpg'),
(36, 48, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:1375;s:4:\"file\";s:31:\"2021/10/background-1-scaled.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"background-1-300x161.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:161;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"background-1-1024x550.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:550;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"background-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"background-1-768x413.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:413;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:25:\"background-1-1536x825.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:825;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:26:\"background-1-2048x1100.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:24:\"background-1-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:24:\"background-1-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:24:\"background-1-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:16:\"background-1.jpg\";}'),
(37, 49, '_wp_attached_file', '2021/10/background-2-scaled.jpg'),
(38, 49, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:1399;s:4:\"file\";s:31:\"2021/10/background-2-scaled.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"background-2-300x164.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:164;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"background-2-1024x559.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:559;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"background-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"background-2-768x420.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:420;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:25:\"background-2-1536x839.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:839;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:26:\"background-2-2048x1119.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1119;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:24:\"background-2-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:24:\"background-2-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:24:\"background-2-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:16:\"background-2.jpg\";}'),
(39, 44, 'content_0_background', '48'),
(40, 44, '_content_0_background', 'field_61799aa2cd580'),
(41, 44, 'content_1_anchor', 'about-us'),
(42, 44, '_content_1_anchor', 'field_61799161bfc84'),
(43, 44, 'content_1_background', '49'),
(44, 44, '_content_1_background', 'field_61799153bfc83'),
(45, 44, 'content_1_title', 'About us'),
(46, 44, '_content_1_title', 'field_6179916fbfc85'),
(47, 44, 'content_1_text', 'Mimir is an innovative software development company that provides a comprehensive suite of services, including software development, mobile app development, FinTech solutions and IT consulting.\r\n\r\nIt’s crucial for us to understand your industry, business processes, and goals to help you to craft a winning tech strategy.'),
(48, 44, '_content_1_text', 'field_61799175bfc86'),
(49, 44, 'content_2_anchor', 'services'),
(50, 44, '_content_2_anchor', 'field_6179919ebfc88'),
(51, 44, 'content_2_small_title', 'How can we help your business?'),
(52, 44, '_content_2_small_title', 'field_617991fcbfc89'),
(53, 44, 'content_2_title', 'Meeting your needs'),
(54, 44, '_content_2_title', 'field_61799205bfc8a'),
(55, 44, 'content_2_image', '51'),
(56, 44, '_content_2_image', 'field_6179920ebfc8b'),
(57, 44, 'content_2_text', 'With years of experience in various tech areas, Mimir ensures faster implementation of the solutions that businesses need to achieve higher ROI.\r\n\r\nWe have successfully built and delivered a range of effective IT solutions worldwide and extended our expertise in enterprise software development, mobile app development, Fintech development and integration.'),
(58, 44, '_content_2_text', 'field_6179921cbfc8c'),
(59, 44, 'content_2_services', '4'),
(60, 44, '_content_2_services', 'field_6179922bbfc8d'),
(61, 50, 'content', 'a:3:{i:0;s:12:\"hero_section\";i:1;s:16:\"about_us_section\";i:2;s:16:\"services_section\";}'),
(62, 50, '_content', 'field_61796d5e7d4ce'),
(63, 50, 'content_0_anchor', 'hero'),
(64, 50, '_content_0_anchor', 'field_61799135bfc81'),
(65, 50, 'content_0_small_title', 'Leading technologies for goal-oriented companies'),
(66, 50, '_content_0_small_title', 'field_61799125bfc7f'),
(67, 50, 'content_0_title', 'Software development partner to accelerate your business'),
(68, 50, '_content_0_title', 'field_6179912bbfc80'),
(69, 50, 'content_0_background', '48'),
(70, 50, '_content_0_background', 'field_61799aa2cd580'),
(71, 50, 'content_1_anchor', 'about-us'),
(72, 50, '_content_1_anchor', 'field_61799161bfc84'),
(73, 50, 'content_1_background', '49'),
(74, 50, '_content_1_background', 'field_61799153bfc83'),
(75, 50, 'content_1_title', 'About us'),
(76, 50, '_content_1_title', 'field_6179916fbfc85'),
(77, 50, 'content_1_text', 'Mimir is an innovative software development company that provides a comprehensive suite of services, including software development, mobile app development, FinTech solutions and IT consulting.\r\n\r\nIt’s crucial for us to understand your industry, business processes, and goals to help you to craft a winning tech strategy.'),
(78, 50, '_content_1_text', 'field_61799175bfc86'),
(79, 50, 'content_2_anchor', ''),
(80, 50, '_content_2_anchor', 'field_6179919ebfc88'),
(81, 50, 'content_2_small_title', ''),
(82, 50, '_content_2_small_title', 'field_617991fcbfc89'),
(83, 50, 'content_2_title', ''),
(84, 50, '_content_2_title', 'field_61799205bfc8a'),
(85, 50, 'content_2_image', ''),
(86, 50, '_content_2_image', 'field_6179920ebfc8b'),
(87, 50, 'content_2_text', ''),
(88, 50, '_content_2_text', 'field_6179921cbfc8c'),
(89, 50, 'content_2_services', ''),
(90, 50, '_content_2_services', 'field_6179922bbfc8d'),
(91, 51, '_wp_attached_file', '2021/10/image-1.jpg'),
(92, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1080;s:6:\"height\";i:998;s:4:\"file\";s:19:\"2021/10/image-1.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"image-1-300x277.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:277;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"image-1-1024x946.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:946;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"image-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"image-1-768x710.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:710;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:19:\"image-1-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:19:\"image-1-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:19:\"image-1-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(93, 52, '_wp_attached_file', '2021/10/service-1.svg'),
(94, 53, '_wp_attached_file', '2021/10/service-2.svg'),
(95, 54, '_wp_attached_file', '2021/10/service-3.svg'),
(96, 55, '_wp_attached_file', '2021/10/service-4.svg'),
(97, 56, '_wp_attached_file', '2021/10/our-highlights-1.svg'),
(98, 57, '_wp_attached_file', '2021/10/our-highlights-2.svg'),
(99, 58, '_wp_attached_file', '2021/10/our-highlights-3.svg'),
(100, 59, '_wp_attached_file', '2021/10/our-highlights-4.svg'),
(101, 44, 'content_2_services_0_icon', '52'),
(102, 44, '_content_2_services_0_icon', 'field_6179924bbfc8f'),
(103, 44, 'content_2_services_0_title', 'Software development'),
(104, 44, '_content_2_services_0_title', 'field_61799245bfc8e'),
(105, 44, 'content_2_services_0_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(106, 44, '_content_2_services_0_text', 'field_61799257bfc90'),
(107, 44, 'content_2_services_1_icon', '53'),
(108, 44, '_content_2_services_1_icon', 'field_6179924bbfc8f'),
(109, 44, 'content_2_services_1_title', 'Mobile app development'),
(110, 44, '_content_2_services_1_title', 'field_61799245bfc8e'),
(111, 44, 'content_2_services_1_text', 'Here at Mimir, we create, design, and deliver intuitive and efficient mobile applications to accommodate your industry-specific business requirements.'),
(112, 44, '_content_2_services_1_text', 'field_61799257bfc90'),
(113, 44, 'content_2_services_2_icon', '54'),
(114, 44, '_content_2_services_2_icon', 'field_6179924bbfc8f'),
(115, 44, 'content_2_services_2_title', 'Fintech'),
(116, 44, '_content_2_services_2_title', 'field_61799245bfc8e'),
(117, 44, 'content_2_services_2_text', 'Our experts provide companies with all kinds of financial technology solutions from payment systems and digital banking solutions to cybersecurity and trading platforms.'),
(118, 44, '_content_2_services_2_text', 'field_61799257bfc90'),
(119, 44, 'content_2_services_3_icon', '55'),
(120, 44, '_content_2_services_3_icon', 'field_6179924bbfc8f'),
(121, 44, 'content_2_services_3_title', 'IT consulting'),
(122, 44, '_content_2_services_3_title', 'field_61799245bfc8e'),
(123, 44, 'content_2_services_3_text', 'We help companies to transform their business and innovate by building comprehensive digital acceleration strategies.'),
(124, 44, '_content_2_services_3_text', 'field_61799257bfc90'),
(125, 44, 'content_3_anchor', 'our-highlights'),
(126, 44, '_content_3_anchor', 'field_61799280bfc92'),
(127, 44, 'content_3_small_title', 'Our highlights'),
(128, 44, '_content_3_small_title', 'field_61799291bfc93'),
(129, 44, 'content_3_title', 'Why choose Mimir'),
(130, 44, '_content_3_title', 'field_61799297bfc94'),
(131, 44, 'content_3_blocks_0_icon', '56'),
(132, 44, '_content_3_blocks_0_icon', 'field_617992b5bfc96'),
(133, 44, 'content_3_blocks_0_title', 'We help to create agile tech solutions'),
(134, 44, '_content_3_blocks_0_title', 'field_617992c1bfc97'),
(135, 44, 'content_3_blocks_1_icon', '57'),
(136, 44, '_content_3_blocks_1_icon', 'field_617992b5bfc96'),
(137, 44, 'content_3_blocks_1_title', 'We create intuitive and engaging interfaces'),
(138, 44, '_content_3_blocks_1_title', 'field_617992c1bfc97'),
(139, 44, 'content_3_blocks_2_icon', '58'),
(140, 44, '_content_3_blocks_2_icon', 'field_617992b5bfc96'),
(141, 44, 'content_3_blocks_2_title', 'We’re aligned with your business goals'),
(142, 44, '_content_3_blocks_2_title', 'field_617992c1bfc97'),
(143, 44, 'content_3_blocks_3_icon', '59'),
(144, 44, '_content_3_blocks_3_icon', 'field_617992b5bfc96'),
(145, 44, 'content_3_blocks_3_title', 'We will help you to expand to new markets'),
(146, 44, '_content_3_blocks_3_title', 'field_617992c1bfc97'),
(147, 44, 'content_3_blocks', '4'),
(148, 44, '_content_3_blocks', 'field_617992a0bfc95'),
(149, 60, 'content', 'a:4:{i:0;s:12:\"hero_section\";i:1;s:16:\"about_us_section\";i:2;s:16:\"services_section\";i:3;s:18:\"highlights_section\";}'),
(150, 60, '_content', 'field_61796d5e7d4ce'),
(151, 60, 'content_0_anchor', 'hero'),
(152, 60, '_content_0_anchor', 'field_61799135bfc81'),
(153, 60, 'content_0_small_title', 'Leading technologies for goal-oriented companies'),
(154, 60, '_content_0_small_title', 'field_61799125bfc7f'),
(155, 60, 'content_0_title', 'Software development partner to accelerate your business'),
(156, 60, '_content_0_title', 'field_6179912bbfc80'),
(157, 60, 'content_0_background', '48'),
(158, 60, '_content_0_background', 'field_61799aa2cd580'),
(159, 60, 'content_1_anchor', 'about-us'),
(160, 60, '_content_1_anchor', 'field_61799161bfc84'),
(161, 60, 'content_1_background', '49'),
(162, 60, '_content_1_background', 'field_61799153bfc83'),
(163, 60, 'content_1_title', 'About us'),
(164, 60, '_content_1_title', 'field_6179916fbfc85'),
(165, 60, 'content_1_text', 'Mimir is an innovative software development company that provides a comprehensive suite of services, including software development, mobile app development, FinTech solutions and IT consulting.\r\n\r\nIt’s crucial for us to understand your industry, business processes, and goals to help you to craft a winning tech strategy.'),
(166, 60, '_content_1_text', 'field_61799175bfc86'),
(167, 60, 'content_2_anchor', 'services'),
(168, 60, '_content_2_anchor', 'field_6179919ebfc88'),
(169, 60, 'content_2_small_title', 'How can we help your business?'),
(170, 60, '_content_2_small_title', 'field_617991fcbfc89'),
(171, 60, 'content_2_title', 'Meeting your needs'),
(172, 60, '_content_2_title', 'field_61799205bfc8a'),
(173, 60, 'content_2_image', '51'),
(174, 60, '_content_2_image', 'field_6179920ebfc8b'),
(175, 60, 'content_2_text', 'With years of experience in various tech areas, Mimir ensures faster implementation of the solutions that businesses need to achieve higher ROI.\r\n\r\nWe have successfully built and delivered a range of effective IT solutions worldwide and extended our expertise in enterprise software development, mobile app development, Fintech development and integration.'),
(176, 60, '_content_2_text', 'field_6179921cbfc8c'),
(177, 60, 'content_2_services', '4'),
(178, 60, '_content_2_services', 'field_6179922bbfc8d'),
(179, 60, 'content_2_services_0_icon', '52'),
(180, 60, '_content_2_services_0_icon', 'field_6179924bbfc8f'),
(181, 60, 'content_2_services_0_title', 'Software development'),
(182, 60, '_content_2_services_0_title', 'field_61799245bfc8e'),
(183, 60, 'content_2_services_0_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(184, 60, '_content_2_services_0_text', 'field_61799257bfc90'),
(185, 60, 'content_2_services_1_icon', '53'),
(186, 60, '_content_2_services_1_icon', 'field_6179924bbfc8f'),
(187, 60, 'content_2_services_1_title', 'Mobile app development'),
(188, 60, '_content_2_services_1_title', 'field_61799245bfc8e'),
(189, 60, 'content_2_services_1_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(190, 60, '_content_2_services_1_text', 'field_61799257bfc90'),
(191, 60, 'content_2_services_2_icon', '54'),
(192, 60, '_content_2_services_2_icon', 'field_6179924bbfc8f'),
(193, 60, 'content_2_services_2_title', 'Fintech'),
(194, 60, '_content_2_services_2_title', 'field_61799245bfc8e'),
(195, 60, 'content_2_services_2_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(196, 60, '_content_2_services_2_text', 'field_61799257bfc90'),
(197, 60, 'content_2_services_3_icon', '55'),
(198, 60, '_content_2_services_3_icon', 'field_6179924bbfc8f'),
(199, 60, 'content_2_services_3_title', 'IT consulting'),
(200, 60, '_content_2_services_3_title', 'field_61799245bfc8e'),
(201, 60, 'content_2_services_3_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(202, 60, '_content_2_services_3_text', 'field_61799257bfc90'),
(203, 60, 'content_3_anchor', 'our-highlights'),
(204, 60, '_content_3_anchor', 'field_61799280bfc92'),
(205, 60, 'content_3_small_title', 'Our highlights'),
(206, 60, '_content_3_small_title', 'field_61799291bfc93'),
(207, 60, 'content_3_title', 'Why choose Mimir'),
(208, 60, '_content_3_title', 'field_61799297bfc94'),
(209, 60, 'content_3_blocks_0_icon', '56'),
(210, 60, '_content_3_blocks_0_icon', 'field_617992b5bfc96'),
(211, 60, 'content_3_blocks_0_title', 'We help to create agile tech solutions'),
(212, 60, '_content_3_blocks_0_title', 'field_617992c1bfc97'),
(213, 60, 'content_3_blocks_1_icon', '57'),
(214, 60, '_content_3_blocks_1_icon', 'field_617992b5bfc96'),
(215, 60, 'content_3_blocks_1_title', 'We create intuitive and engaging interfaces'),
(216, 60, '_content_3_blocks_1_title', 'field_617992c1bfc97'),
(217, 60, 'content_3_blocks_2_icon', '58'),
(218, 60, '_content_3_blocks_2_icon', 'field_617992b5bfc96'),
(219, 60, 'content_3_blocks_2_title', 'We’re aligned with your business goals'),
(220, 60, '_content_3_blocks_2_title', 'field_617992c1bfc97'),
(221, 60, 'content_3_blocks_3_icon', '59'),
(222, 60, '_content_3_blocks_3_icon', 'field_617992b5bfc96'),
(223, 60, 'content_3_blocks_3_title', 'We will help you to expand to new markets'),
(224, 60, '_content_3_blocks_3_title', 'field_617992c1bfc97'),
(225, 60, 'content_3_blocks', '4'),
(226, 60, '_content_3_blocks', 'field_617992a0bfc95'),
(227, 61, 'content', 'a:4:{i:0;s:12:\"hero_section\";i:1;s:16:\"about_us_section\";i:2;s:16:\"services_section\";i:3;s:18:\"highlights_section\";}'),
(228, 61, '_content', 'field_61796d5e7d4ce'),
(229, 61, 'content_0_anchor', 'hero'),
(230, 61, '_content_0_anchor', 'field_61799135bfc81'),
(231, 61, 'content_0_small_title', 'Leading technologies for goal-oriented companies'),
(232, 61, '_content_0_small_title', 'field_61799125bfc7f'),
(233, 61, 'content_0_title', 'Software development partner to accelerate your business'),
(234, 61, '_content_0_title', 'field_6179912bbfc80'),
(235, 61, 'content_0_background', '48'),
(236, 61, '_content_0_background', 'field_61799aa2cd580'),
(237, 61, 'content_1_anchor', 'about-us'),
(238, 61, '_content_1_anchor', 'field_61799161bfc84'),
(239, 61, 'content_1_background', '49'),
(240, 61, '_content_1_background', 'field_61799153bfc83'),
(241, 61, 'content_1_title', 'About us'),
(242, 61, '_content_1_title', 'field_6179916fbfc85'),
(243, 61, 'content_1_text', 'Mimir is an innovative software development company that provides a comprehensive suite of services, including software development, mobile app development, FinTech solutions and IT consulting.\r\n\r\nIt’s crucial for us to understand your industry, business processes, and goals to help you to craft a winning tech strategy.'),
(244, 61, '_content_1_text', 'field_61799175bfc86'),
(245, 61, 'content_2_anchor', 'services'),
(246, 61, '_content_2_anchor', 'field_6179919ebfc88'),
(247, 61, 'content_2_small_title', 'How can we help your business?'),
(248, 61, '_content_2_small_title', 'field_617991fcbfc89'),
(249, 61, 'content_2_title', 'Meeting your needs'),
(250, 61, '_content_2_title', 'field_61799205bfc8a'),
(251, 61, 'content_2_image', '51'),
(252, 61, '_content_2_image', 'field_6179920ebfc8b'),
(253, 61, 'content_2_text', 'With years of experience in various tech areas, Mimir ensures faster implementation of the solutions that businesses need to achieve higher ROI.\r\n\r\nWe have successfully built and delivered a range of effective IT solutions worldwide and extended our expertise in enterprise software development, mobile app development, Fintech development and integration.'),
(254, 61, '_content_2_text', 'field_6179921cbfc8c'),
(255, 61, 'content_2_services', '4'),
(256, 61, '_content_2_services', 'field_6179922bbfc8d'),
(257, 61, 'content_2_services_0_icon', '52'),
(258, 61, '_content_2_services_0_icon', 'field_6179924bbfc8f'),
(259, 61, 'content_2_services_0_title', 'Software development'),
(260, 61, '_content_2_services_0_title', 'field_61799245bfc8e'),
(261, 61, 'content_2_services_0_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(262, 61, '_content_2_services_0_text', 'field_61799257bfc90'),
(263, 61, 'content_2_services_1_icon', '53'),
(264, 61, '_content_2_services_1_icon', 'field_6179924bbfc8f'),
(265, 61, 'content_2_services_1_title', 'Mobile app development'),
(266, 61, '_content_2_services_1_title', 'field_61799245bfc8e'),
(267, 61, 'content_2_services_1_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(268, 61, '_content_2_services_1_text', 'field_61799257bfc90'),
(269, 61, 'content_2_services_2_icon', '54'),
(270, 61, '_content_2_services_2_icon', 'field_6179924bbfc8f'),
(271, 61, 'content_2_services_2_title', 'Fintech'),
(272, 61, '_content_2_services_2_title', 'field_61799245bfc8e'),
(273, 61, 'content_2_services_2_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(274, 61, '_content_2_services_2_text', 'field_61799257bfc90'),
(275, 61, 'content_2_services_3_icon', '55'),
(276, 61, '_content_2_services_3_icon', 'field_6179924bbfc8f'),
(277, 61, 'content_2_services_3_title', 'IT consulting'),
(278, 61, '_content_2_services_3_title', 'field_61799245bfc8e'),
(279, 61, 'content_2_services_3_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(280, 61, '_content_2_services_3_text', 'field_61799257bfc90'),
(281, 61, 'content_3_anchor', 'our-highlights'),
(282, 61, '_content_3_anchor', 'field_61799280bfc92'),
(283, 61, 'content_3_small_title', 'Our highlights'),
(284, 61, '_content_3_small_title', 'field_61799291bfc93'),
(285, 61, 'content_3_title', 'Why choose Mimir'),
(286, 61, '_content_3_title', 'field_61799297bfc94'),
(287, 61, 'content_3_blocks_0_icon', '56'),
(288, 61, '_content_3_blocks_0_icon', 'field_617992b5bfc96'),
(289, 61, 'content_3_blocks_0_title', 'We help to create agile tech solutions'),
(290, 61, '_content_3_blocks_0_title', 'field_617992c1bfc97'),
(291, 61, 'content_3_blocks_1_icon', '57'),
(292, 61, '_content_3_blocks_1_icon', 'field_617992b5bfc96'),
(293, 61, 'content_3_blocks_1_title', 'We create intuitive and engaging interfaces'),
(294, 61, '_content_3_blocks_1_title', 'field_617992c1bfc97'),
(295, 61, 'content_3_blocks_2_icon', '58'),
(296, 61, '_content_3_blocks_2_icon', 'field_617992b5bfc96'),
(297, 61, 'content_3_blocks_2_title', 'We’re aligned with your business goals'),
(298, 61, '_content_3_blocks_2_title', 'field_617992c1bfc97'),
(299, 61, 'content_3_blocks_3_icon', '59'),
(300, 61, '_content_3_blocks_3_icon', 'field_617992b5bfc96'),
(301, 61, 'content_3_blocks_3_title', 'We will help you to expand to new markets'),
(302, 61, '_content_3_blocks_3_title', 'field_617992c1bfc97'),
(303, 61, 'content_3_blocks', '4'),
(304, 61, '_content_3_blocks', 'field_617992a0bfc95'),
(323, 64, '_menu_item_type', 'custom'),
(324, 64, '_menu_item_menu_item_parent', '0'),
(325, 64, '_menu_item_object_id', '64'),
(326, 64, '_menu_item_object', 'custom'),
(327, 64, '_menu_item_target', ''),
(328, 64, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(329, 64, '_menu_item_xfn', ''),
(330, 64, '_menu_item_url', '#about-us'),
(332, 65, '_menu_item_type', 'custom'),
(333, 65, '_menu_item_menu_item_parent', '0'),
(334, 65, '_menu_item_object_id', '65'),
(335, 65, '_menu_item_object', 'custom'),
(336, 65, '_menu_item_target', ''),
(337, 65, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(338, 65, '_menu_item_xfn', ''),
(339, 65, '_menu_item_url', '#services'),
(341, 66, '_menu_item_type', 'custom'),
(342, 66, '_menu_item_menu_item_parent', '0'),
(343, 66, '_menu_item_object_id', '66'),
(344, 66, '_menu_item_object', 'custom'),
(345, 66, '_menu_item_target', ''),
(346, 66, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(347, 66, '_menu_item_xfn', ''),
(348, 66, '_menu_item_url', '#our-highlights'),
(350, 67, '_menu_item_type', 'custom'),
(351, 67, '_menu_item_menu_item_parent', '0'),
(352, 67, '_menu_item_object_id', '67'),
(353, 67, '_menu_item_object', 'custom'),
(354, 67, '_menu_item_target', ''),
(355, 67, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(356, 67, '_menu_item_xfn', ''),
(357, 67, '_menu_item_url', '#contact'),
(359, 5, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(360, 64, '_wp_old_date', '2021-10-27'),
(361, 65, '_wp_old_date', '2021-10-27'),
(362, 66, '_wp_old_date', '2021-10-27'),
(363, 67, '_wp_old_date', '2021-10-27'),
(364, 68, 'content', 'a:4:{i:0;s:12:\"hero_section\";i:1;s:16:\"about_us_section\";i:2;s:16:\"services_section\";i:3;s:18:\"highlights_section\";}'),
(365, 68, '_content', 'field_61796d5e7d4ce'),
(366, 68, 'content_0_anchor', 'hero'),
(367, 68, '_content_0_anchor', 'field_61799135bfc81'),
(368, 68, 'content_0_small_title', 'Leading technologies for goal-oriented companies'),
(369, 68, '_content_0_small_title', 'field_61799125bfc7f'),
(370, 68, 'content_0_title', 'Software development partner to accelerate your business'),
(371, 68, '_content_0_title', 'field_6179912bbfc80'),
(372, 68, 'content_0_background', '48'),
(373, 68, '_content_0_background', 'field_61799aa2cd580'),
(374, 68, 'content_1_anchor', 'about-us'),
(375, 68, '_content_1_anchor', 'field_61799161bfc84'),
(376, 68, 'content_1_background', '49'),
(377, 68, '_content_1_background', 'field_61799153bfc83'),
(378, 68, 'content_1_title', 'About us'),
(379, 68, '_content_1_title', 'field_6179916fbfc85'),
(380, 68, 'content_1_text', 'Mimir is an innovative software development company that provides a comprehensive suite of services, including software development, mobile app development, FinTech solutions and IT consulting.\r\n\r\nIt’s crucial for us to understand your industry, business processes, and goals to help you to craft a winning tech strategy.'),
(381, 68, '_content_1_text', 'field_61799175bfc86'),
(382, 68, 'content_2_anchor', 'services'),
(383, 68, '_content_2_anchor', 'field_6179919ebfc88'),
(384, 68, 'content_2_small_title', 'How can we help your business?'),
(385, 68, '_content_2_small_title', 'field_617991fcbfc89'),
(386, 68, 'content_2_title', 'Meeting your needs'),
(387, 68, '_content_2_title', 'field_61799205bfc8a'),
(388, 68, 'content_2_image', '51'),
(389, 68, '_content_2_image', 'field_6179920ebfc8b'),
(390, 68, 'content_2_text', 'With years of experience in various tech areas, Mimir ensures faster implementation of the solutions that businesses need to achieve higher ROI.\r\n\r\nWe have successfully built and delivered a range of effective IT solutions worldwide and extended our expertise in enterprise software development, mobile app development, Fintech development and integration.'),
(391, 68, '_content_2_text', 'field_6179921cbfc8c'),
(392, 68, 'content_2_services', '4'),
(393, 68, '_content_2_services', 'field_6179922bbfc8d'),
(394, 68, 'content_2_services_0_icon', '52'),
(395, 68, '_content_2_services_0_icon', 'field_6179924bbfc8f'),
(396, 68, 'content_2_services_0_title', 'Software development'),
(397, 68, '_content_2_services_0_title', 'field_61799245bfc8e'),
(398, 68, 'content_2_services_0_text', 'Our expert team develops agile software solutions of different complexity to reach desired business results.'),
(399, 68, '_content_2_services_0_text', 'field_61799257bfc90'),
(400, 68, 'content_2_services_1_icon', '53'),
(401, 68, '_content_2_services_1_icon', 'field_6179924bbfc8f'),
(402, 68, 'content_2_services_1_title', 'Mobile app development'),
(403, 68, '_content_2_services_1_title', 'field_61799245bfc8e'),
(404, 68, 'content_2_services_1_text', 'Here at Mimir, we create, design, and deliver intuitive and efficient mobile applications to accommodate your industry-specific business requirements.'),
(405, 68, '_content_2_services_1_text', 'field_61799257bfc90'),
(406, 68, 'content_2_services_2_icon', '54'),
(407, 68, '_content_2_services_2_icon', 'field_6179924bbfc8f'),
(408, 68, 'content_2_services_2_title', 'Fintech'),
(409, 68, '_content_2_services_2_title', 'field_61799245bfc8e'),
(410, 68, 'content_2_services_2_text', 'Our experts provide companies with all kinds of financial technology solutions from payment systems and digital banking solutions to cybersecurity and trading platforms.'),
(411, 68, '_content_2_services_2_text', 'field_61799257bfc90'),
(412, 68, 'content_2_services_3_icon', '55'),
(413, 68, '_content_2_services_3_icon', 'field_6179924bbfc8f'),
(414, 68, 'content_2_services_3_title', 'IT consulting'),
(415, 68, '_content_2_services_3_title', 'field_61799245bfc8e'),
(416, 68, 'content_2_services_3_text', 'We help companies to transform their business and innovate by building comprehensive digital acceleration strategies.'),
(417, 68, '_content_2_services_3_text', 'field_61799257bfc90'),
(418, 68, 'content_3_anchor', 'our-highlights'),
(419, 68, '_content_3_anchor', 'field_61799280bfc92'),
(420, 68, 'content_3_small_title', 'Our highlights'),
(421, 68, '_content_3_small_title', 'field_61799291bfc93'),
(422, 68, 'content_3_title', 'Why choose Mimir'),
(423, 68, '_content_3_title', 'field_61799297bfc94'),
(424, 68, 'content_3_blocks_0_icon', '56'),
(425, 68, '_content_3_blocks_0_icon', 'field_617992b5bfc96'),
(426, 68, 'content_3_blocks_0_title', 'We help to create agile tech solutions'),
(427, 68, '_content_3_blocks_0_title', 'field_617992c1bfc97'),
(428, 68, 'content_3_blocks_1_icon', '57'),
(429, 68, '_content_3_blocks_1_icon', 'field_617992b5bfc96'),
(430, 68, 'content_3_blocks_1_title', 'We create intuitive and engaging interfaces'),
(431, 68, '_content_3_blocks_1_title', 'field_617992c1bfc97'),
(432, 68, 'content_3_blocks_2_icon', '58'),
(433, 68, '_content_3_blocks_2_icon', 'field_617992b5bfc96'),
(434, 68, 'content_3_blocks_2_title', 'We’re aligned with your business goals'),
(435, 68, '_content_3_blocks_2_title', 'field_617992c1bfc97'),
(436, 68, 'content_3_blocks_3_icon', '59'),
(437, 68, '_content_3_blocks_3_icon', 'field_617992b5bfc96'),
(438, 68, 'content_3_blocks_3_title', 'We will help you to expand to new markets'),
(439, 68, '_content_3_blocks_3_title', 'field_617992c1bfc97'),
(440, 68, 'content_3_blocks', '4'),
(441, 68, '_content_3_blocks', 'field_617992a0bfc95'),
(442, 70, '_wp_attached_file', '2021/11/favicon.png'),
(443, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:513;s:6:\"height\";i:512;s:4:\"file\";s:19:\"2021/11/favicon.png\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"favicon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"favicon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:19:\"favicon-360x160.png\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:19:\"favicon-360x240.png\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:19:\"favicon-513x436.png\";s:5:\"width\";i:513;s:6:\"height\";i:436;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(444, 71, '_wp_attached_file', '2021/11/cropped-favicon.png'),
(445, 71, '_wp_attachment_context', 'site-icon'),
(446, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:27:\"2021/11/cropped-favicon.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-360x160.png\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-360x240.png\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-512x436.png\";s:5:\"width\";i:512;s:6:\"height\";i:436;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:25:\"cropped-favicon-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Структура таблиці `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-10-27 15:13:20', '2021-10-27 15:13:20', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2021-10-27 15:13:20', '2021-10-27 15:13:20', '', 0, 'http://mimir-wp/build/?p=1', 0, 'post', '', 1),
(2, 1, '2021-10-27 15:13:20', '2021-10-27 15:13:20', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://mimir-wp/build/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2021-10-27 15:13:20', '2021-10-27 15:13:20', '', 0, 'http://mimir-wp/build/?page_id=2', 0, 'page', '', 0),
(3, 1, '2021-10-27 15:13:20', '2021-10-27 15:13:20', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Our website address is: http://mimir-wp/build.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Comments</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Media</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cookies</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Embedded content from other websites</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you request a password reset, your IP address will be included in the reset email.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-10-27 15:13:20', '2021-10-27 15:13:20', '', 0, 'http://mimir-wp/build/?page_id=3', 0, 'page', '', 0),
(5, 1, '2021-10-27 15:13:50', '2021-10-27 15:13:50', '<div class=\"field__group\">[text* text-23 class:form__field placeholder \"Your Name\"]</div>\r\n<div class=\"field__group\">[email* email-89 class:form__field placeholder \"Your Email\"]</div>\r\n<div class=\"field__group\">[textarea* textarea-835 class:form__field placeholder \"Your Message\"]</div>\r\n<div class=\"submit__group\">[submit class:submit__field \"Send\"]</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@mimir-wp>\n[_site_admin_email]\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@mimir-wp>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact form', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2021-10-30 15:18:10', '2021-10-30 15:18:10', '', 0, 'http://mimir-wp/build/?post_type=wpcf7_contact_form&#038;p=5', 0, 'wpcf7_contact_form', '', 0),
(6, 1, '2021-10-27 15:16:55', '2021-10-27 15:16:55', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Page', 'page', 'publish', 'closed', 'closed', '', 'group_61796d585af96', '', '', '2021-10-28 21:01:24', '2021-10-28 21:01:24', '', 0, 'http://mimir-wp/build/?post_type=acf-field-group&#038;p=6', 0, 'acf-field-group', '', 0),
(7, 1, '2021-10-27 15:16:55', '2021-10-27 15:16:55', 'a:9:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"layouts\";a:4:{s:20:\"layout_61796d65000fc\";a:6:{s:3:\"key\";s:20:\"layout_61796d65000fc\";s:5:\"label\";s:12:\"Hero section\";s:4:\"name\";s:12:\"hero_section\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:20:\"layout_61799146bfc82\";a:6:{s:3:\"key\";s:20:\"layout_61799146bfc82\";s:5:\"label\";s:16:\"About Us Section\";s:4:\"name\";s:16:\"about_us_section\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:20:\"layout_61799192bfc87\";a:6:{s:3:\"key\";s:20:\"layout_61799192bfc87\";s:5:\"label\";s:16:\"Services Section\";s:4:\"name\";s:16:\"services_section\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:20:\"layout_61799271bfc91\";a:6:{s:3:\"key\";s:20:\"layout_61799271bfc91\";s:5:\"label\";s:18:\"Highlights Section\";s:4:\"name\";s:18:\"highlights_section\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:11:\"Add Content\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_61796d5e7d4ce', '', '', '2021-10-27 18:29:12', '2021-10-27 18:29:12', '', 6, 'http://mimir-wp/build/?post_type=acf-field&#038;p=7', 0, 'acf-field', '', 0),
(8, 1, '2021-10-27 15:17:13', '2021-10-27 15:17:13', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"theme-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Theme Settings', 'theme-settings', 'publish', 'closed', 'closed', '', 'group_61796d6b71a8a', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 0, 'http://mimir-wp/build/?post_type=acf-field-group&#038;p=8', 0, 'acf-field-group', '', 0),
(9, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61796d65000fc\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:1:\"#\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Anchor', 'anchor', 'publish', 'closed', 'closed', '', 'field_61799135bfc81', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=9', 0, 'acf-field', '', 0),
(10, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61796d65000fc\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Small title', 'small_title', 'publish', 'closed', 'closed', '', 'field_61799125bfc7f', '', '', '2021-10-27 18:30:07', '2021-10-27 18:30:07', '', 7, 'http://mimir-wp/build/?post_type=acf-field&#038;p=10', 2, 'acf-field', '', 0),
(11, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61796d65000fc\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_6179912bbfc80', '', '', '2021-10-27 18:30:07', '2021-10-27 18:30:07', '', 7, 'http://mimir-wp/build/?post_type=acf-field&#038;p=11', 3, 'acf-field', '', 0),
(12, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799146bfc82\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:1:\"#\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Anchor', 'anchor', 'publish', 'closed', 'closed', '', 'field_61799161bfc84', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=12', 0, 'acf-field', '', 0),
(13, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799146bfc82\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Background', 'background', 'publish', 'closed', 'closed', '', 'field_61799153bfc83', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=13', 1, 'acf-field', '', 0),
(14, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799146bfc82\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_6179916fbfc85', '', '', '2021-10-27 18:30:07', '2021-10-27 18:30:07', '', 7, 'http://mimir-wp/build/?post_type=acf-field&#038;p=14', 2, 'acf-field', '', 0),
(15, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799146bfc82\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Text', 'text', 'publish', 'closed', 'closed', '', 'field_61799175bfc86', '', '', '2021-10-27 18:30:07', '2021-10-27 18:30:07', '', 7, 'http://mimir-wp/build/?post_type=acf-field&#038;p=15', 3, 'acf-field', '', 0),
(16, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799192bfc87\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:1:\"#\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Anchor', 'anchor', 'publish', 'closed', 'closed', '', 'field_6179919ebfc88', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=16', 0, 'acf-field', '', 0),
(17, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799192bfc87\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Small title', 'small_title', 'publish', 'closed', 'closed', '', 'field_617991fcbfc89', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=17', 1, 'acf-field', '', 0),
(18, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799192bfc87\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_61799205bfc8a', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=18', 2, 'acf-field', '', 0),
(19, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799192bfc87\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_6179920ebfc8b', '', '', '2021-10-28 21:01:24', '2021-10-28 21:01:24', '', 7, 'http://mimir-wp/build/?post_type=acf-field&#038;p=19', 3, 'acf-field', '', 0),
(20, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799192bfc87\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Text', 'text', 'publish', 'closed', 'closed', '', 'field_6179921cbfc8c', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=20', 4, 'acf-field', '', 0),
(21, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799192bfc87\";s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:11:\"Add Service\";}', 'Services', 'services', 'publish', 'closed', 'closed', '', 'field_6179922bbfc8d', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=21', 5, 'acf-field', '', 0),
(22, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_6179924bbfc8f', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 21, 'http://mimir-wp/build/?post_type=acf-field&p=22', 0, 'acf-field', '', 0),
(23, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_61799245bfc8e', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 21, 'http://mimir-wp/build/?post_type=acf-field&p=23', 1, 'acf-field', '', 0),
(24, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Text', 'text', 'publish', 'closed', 'closed', '', 'field_61799257bfc90', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 21, 'http://mimir-wp/build/?post_type=acf-field&p=24', 2, 'acf-field', '', 0),
(25, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799271bfc91\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:1:\"#\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Anchor', 'anchor', 'publish', 'closed', 'closed', '', 'field_61799280bfc92', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=25', 0, 'acf-field', '', 0),
(26, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799271bfc91\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Small title', 'small_title', 'publish', 'closed', 'closed', '', 'field_61799291bfc93', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=26', 1, 'acf-field', '', 0),
(27, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799271bfc91\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_61799297bfc94', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=27', 2, 'acf-field', '', 0),
(28, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:11:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61799271bfc91\";s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:9:\"Add Block\";}', 'Blocks', 'blocks', 'publish', 'closed', 'closed', '', 'field_617992a0bfc95', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=28', 3, 'acf-field', '', 0),
(29, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_617992b5bfc96', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 28, 'http://mimir-wp/build/?post_type=acf-field&p=29', 0, 'acf-field', '', 0),
(30, 1, '2021-10-27 17:56:42', '2021-10-27 17:56:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_617992c1bfc97', '', '', '2021-10-27 17:56:42', '2021-10-27 17:56:42', '', 28, 'http://mimir-wp/build/?post_type=acf-field&p=30', 1, 'acf-field', '', 0),
(31, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:4:\"left\";s:8:\"endpoint\";i:0;}', 'Header', 'header', 'publish', 'closed', 'closed', '', 'field_617992eb1e1bc', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 8, 'http://mimir-wp/build/?post_type=acf-field&p=31', 0, 'acf-field', '', 0),
(32, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Logo', 'logo', 'publish', 'closed', 'closed', '', 'field_617993011e1bd', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 8, 'http://mimir-wp/build/?post_type=acf-field&p=32', 1, 'acf-field', '', 0),
(33, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:4:\"left\";s:8:\"endpoint\";i:0;}', 'Footer', 'footer', 'publish', 'closed', 'closed', '', 'field_617993131e1be', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 8, 'http://mimir-wp/build/?post_type=acf-field&p=33', 2, 'acf-field', '', 0),
(34, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Footer', 'footer', 'publish', 'closed', 'closed', '', 'field_617993371e1bf', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 8, 'http://mimir-wp/build/?post_type=acf-field&p=34', 3, 'acf-field', '', 0),
(35, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_617993451e1c0', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 34, 'http://mimir-wp/build/?post_type=acf-field&p=35', 0, 'acf-field', '', 0),
(36, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:3;s:9:\"new_lines\";s:7:\"wpautop\";}', 'Address', 'address', 'publish', 'closed', 'closed', '', 'field_6179935d1e1c1', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 34, 'http://mimir-wp/build/?post_type=acf-field&p=36', 1, 'acf-field', '', 0),
(37, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'Email', 'email', 'publish', 'closed', 'closed', '', 'field_6179936f1e1c2', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 34, 'http://mimir-wp/build/?post_type=acf-field&p=37', 2, 'acf-field', '', 0),
(38, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Phone', 'phone', 'publish', 'closed', 'closed', '', 'field_6179937e1e1c3', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 34, 'http://mimir-wp/build/?post_type=acf-field&p=38', 3, 'acf-field', '', 0),
(39, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Logo', 'logo', 'publish', 'closed', 'closed', '', 'field_617993921e1c4', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 34, 'http://mimir-wp/build/?post_type=acf-field&p=39', 4, 'acf-field', '', 0),
(40, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Copyrights', 'copyrights', 'publish', 'closed', 'closed', '', 'field_6179939d1e1c5', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 34, 'http://mimir-wp/build/?post_type=acf-field&p=40', 5, 'acf-field', '', 0),
(41, 1, '2021-10-27 18:00:32', '2021-10-27 18:00:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Form shortcode', 'form_shortcode', 'publish', 'closed', 'closed', '', 'field_617993b21e1c6', '', '', '2021-10-27 18:00:32', '2021-10-27 18:00:32', '', 34, 'http://mimir-wp/build/?post_type=acf-field&p=41', 6, 'acf-field', '', 0),
(42, 1, '2021-10-27 18:26:15', '2021-10-27 18:26:15', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2021-10-27 18:26:15', '2021-10-27 18:26:15', '', 0, 'http://mimir-wp/build/wp-content/uploads/2021/10/logo.svg', 0, 'attachment', 'image/svg+xml', 0),
(43, 1, '2021-10-27 18:27:45', '2021-10-27 18:27:45', '', 'footer-logo', '', 'inherit', 'open', 'closed', '', 'footer-logo', '', '', '2021-10-27 18:27:45', '2021-10-27 18:27:45', '', 0, 'http://mimir-wp/build/wp-content/uploads/2021/10/footer-logo.svg', 0, 'attachment', 'image/svg+xml', 0),
(44, 1, '2021-10-27 18:29:20', '2021-10-27 18:29:20', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2021-10-31 20:28:31', '2021-10-31 20:28:31', '', 0, 'http://mimir-wp/build/?page_id=44', 0, 'page', '', 0),
(45, 1, '2021-10-27 18:29:20', '2021-10-27 18:29:20', '', 'Home', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2021-10-27 18:29:20', '2021-10-27 18:29:20', '', 44, 'http://mimir-wp/build/?p=45', 0, 'revision', '', 0),
(46, 1, '2021-10-27 18:29:50', '2021-10-27 18:29:50', '', 'Home', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2021-10-27 18:29:50', '2021-10-27 18:29:50', '', 44, 'http://mimir-wp/build/?p=46', 0, 'revision', '', 0),
(47, 1, '2021-10-27 18:30:07', '2021-10-27 18:30:07', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61796d65000fc\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Background', 'background', 'publish', 'closed', 'closed', '', 'field_61799aa2cd580', '', '', '2021-10-27 18:30:07', '2021-10-27 18:30:07', '', 7, 'http://mimir-wp/build/?post_type=acf-field&p=47', 1, 'acf-field', '', 0),
(48, 1, '2021-10-27 18:30:38', '2021-10-27 18:30:38', '', 'background-1', '', 'inherit', 'open', 'closed', '', 'background-1', '', '', '2021-10-27 18:30:38', '2021-10-27 18:30:38', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/background-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(49, 1, '2021-10-27 18:31:13', '2021-10-27 18:31:13', '', 'background-2', '', 'inherit', 'open', 'closed', '', 'background-2', '', '', '2021-10-27 18:31:13', '2021-10-27 18:31:13', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/background-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(50, 1, '2021-10-27 18:31:35', '2021-10-27 18:31:35', '', 'Home', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2021-10-27 18:31:35', '2021-10-27 18:31:35', '', 44, 'http://mimir-wp/build/?p=50', 0, 'revision', '', 0),
(51, 1, '2021-10-27 18:32:15', '2021-10-27 18:32:15', '', 'image-1', '', 'inherit', 'open', 'closed', '', 'image-1', '', '', '2021-10-27 18:32:15', '2021-10-27 18:32:15', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/image-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(52, 1, '2021-10-27 18:34:50', '2021-10-27 18:34:50', '', 'service-1', '', 'inherit', 'open', 'closed', '', 'service-1', '', '', '2021-10-27 18:34:50', '2021-10-27 18:34:50', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/service-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(53, 1, '2021-10-27 18:34:50', '2021-10-27 18:34:50', '', 'service-2', '', 'inherit', 'open', 'closed', '', 'service-2', '', '', '2021-10-27 18:34:50', '2021-10-27 18:34:50', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/service-2.svg', 0, 'attachment', 'image/svg+xml', 0),
(54, 1, '2021-10-27 18:34:50', '2021-10-27 18:34:50', '', 'service-3', '', 'inherit', 'open', 'closed', '', 'service-3', '', '', '2021-10-27 18:34:50', '2021-10-27 18:34:50', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/service-3.svg', 0, 'attachment', 'image/svg+xml', 0),
(55, 1, '2021-10-27 18:34:51', '2021-10-27 18:34:51', '', 'service-4', '', 'inherit', 'open', 'closed', '', 'service-4', '', '', '2021-10-27 18:34:51', '2021-10-27 18:34:51', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/service-4.svg', 0, 'attachment', 'image/svg+xml', 0),
(56, 1, '2021-10-27 18:38:30', '2021-10-27 18:38:30', '', 'our-highlights-1', '', 'inherit', 'open', 'closed', '', 'our-highlights-1', '', '', '2021-10-27 18:38:30', '2021-10-27 18:38:30', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/our-highlights-1.svg', 0, 'attachment', 'image/svg+xml', 0),
(57, 1, '2021-10-27 18:38:30', '2021-10-27 18:38:30', '', 'our-highlights-2', '', 'inherit', 'open', 'closed', '', 'our-highlights-2', '', '', '2021-10-27 18:38:30', '2021-10-27 18:38:30', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/our-highlights-2.svg', 0, 'attachment', 'image/svg+xml', 0),
(58, 1, '2021-10-27 18:38:31', '2021-10-27 18:38:31', '', 'our-highlights-3', '', 'inherit', 'open', 'closed', '', 'our-highlights-3', '', '', '2021-10-27 18:38:31', '2021-10-27 18:38:31', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/our-highlights-3.svg', 0, 'attachment', 'image/svg+xml', 0),
(59, 1, '2021-10-27 18:38:31', '2021-10-27 18:38:31', '', 'our-highlights-4', '', 'inherit', 'open', 'closed', '', 'our-highlights-4', '', '', '2021-10-27 18:38:31', '2021-10-27 18:38:31', '', 44, 'http://mimir-wp/build/wp-content/uploads/2021/10/our-highlights-4.svg', 0, 'attachment', 'image/svg+xml', 0),
(60, 1, '2021-10-27 18:39:15', '2021-10-27 18:39:15', '', 'Home', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2021-10-27 18:39:15', '2021-10-27 18:39:15', '', 44, 'http://mimir-wp/build/?p=60', 0, 'revision', '', 0),
(61, 1, '2021-10-27 18:59:19', '2021-10-27 18:59:19', '', 'Home', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2021-10-27 18:59:19', '2021-10-27 18:59:19', '', 44, 'http://mimir-wp/build/?p=61', 0, 'revision', '', 0),
(64, 1, '2021-10-30 16:57:24', '2021-10-27 19:14:36', '', 'about us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2021-10-30 16:57:24', '2021-10-30 16:57:24', '', 0, 'http://mimir-wp/build/?p=64', 1, 'nav_menu_item', '', 0),
(65, 1, '2021-10-30 16:57:24', '2021-10-27 19:16:52', '', 'services', '', 'publish', 'closed', 'closed', '', 'services', '', '', '2021-10-30 16:57:24', '2021-10-30 16:57:24', '', 0, 'http://mimir-wp/build/?p=65', 2, 'nav_menu_item', '', 0),
(66, 1, '2021-10-30 16:57:24', '2021-10-27 19:16:52', '', 'our highlights', '', 'publish', 'closed', 'closed', '', 'our-highlights', '', '', '2021-10-30 16:57:24', '2021-10-30 16:57:24', '', 0, 'http://mimir-wp/build/?p=66', 3, 'nav_menu_item', '', 0),
(67, 1, '2021-10-30 16:57:24', '2021-10-27 19:16:52', '', 'contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2021-10-30 16:57:24', '2021-10-30 16:57:24', '', 0, 'http://mimir-wp/build/?p=67', 4, 'nav_menu_item', '', 0),
(68, 1, '2021-10-31 20:28:31', '2021-10-31 20:28:31', '', 'Home', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2021-10-31 20:28:31', '2021-10-31 20:28:31', '', 44, 'http://mimir-wp/build/?p=68', 0, 'revision', '', 0),
(70, 1, '2021-11-06 11:24:16', '2021-11-06 11:24:16', '', 'favicon', '', 'inherit', 'open', 'closed', '', 'favicon', '', '', '2021-11-06 11:24:16', '2021-11-06 11:24:16', '', 0, 'http://mimir-wp/build/wp-content/uploads/2021/11/favicon.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2021-11-06 11:24:20', '2021-11-06 11:24:20', 'http://mimir-wp/build/wp-content/uploads/2021/11/cropped-favicon.png', 'cropped-favicon.png', '', 'inherit', 'open', 'closed', '', 'cropped-favicon-png', '', '', '2021-11-06 11:24:20', '2021-11-06 11:24:20', '', 0, 'http://mimir-wp/build/wp-content/uploads/2021/11/cropped-favicon.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main menu', 'main-menu', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(64, 2, 0),
(65, 2, 0),
(66, 2, 0),
(67, 2, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 4);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '69'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce'),
(20, 1, 'wp_user-settings-time', '1635359490'),
(21, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(23, 1, 'nav_menu_recently_edited', '2'),
(25, 1, 'session_tokens', 'a:1:{s:64:\"89d8080b96af679e95048acb9ff3bcc1d91e160a8da30410adfe9ea4934f5d7b\";a:4:{s:10:\"expiration\";i:1637407399;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36\";s:5:\"login\";i:1636197799;}}');

-- --------------------------------------------------------

--
-- Структура таблиці `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$B5vT4Ogb4ijf7xyHXiIqeblIUIuQaX0', 'admin', 'bruskevtsevr@gmail.com', 'http://mimir-wp/build', '2021-10-27 15:13:20', '', 0, 'admin');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Індекси таблиці `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Індекси таблиці `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Індекси таблиці `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Індекси таблиці `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Індекси таблиці `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Індекси таблиці `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Індекси таблиці `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=286;

--
-- AUTO_INCREMENT для таблиці `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=449;

--
-- AUTO_INCREMENT для таблиці `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT для таблиці `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблиці `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблиці `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблиці `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
